<!DOCTYPE html>
<?php
if ((isset($_POST['id'])) && ($_POST['nombre'] != '') && ($_POST['apellidos'] != '')($_POST['cedula'] != '')
&& ($_POST['direccion'] != '')&& ($_POST['telefono'] != '')&& ($_POST['fecha_nacimiento'] != '')&& ($_POST['email'] != '')) {

    include "models/modelo.php";
    $nuevo = new Service();
    $asd = $nuevo->setServicio($_POST['id'], $_POST['nombre'], $_POST['apellidos'], $_POST['cedula']
    , $_POST['direccion'], $_POST['telefono'], $_POST['fecha_nacimiento'], $_POST['email']);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo MVC con PHP</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">

                    <form action="#" method="post" class="col-lg-5">
                        <h3>Nuevo servicio</h3>                
                        Id: <input type="text" name="id" class="form-control"/>    
                        Nombre: <input type="text" name="nombre" class="form-control"/>  
                        Apellidos: <input type="text" name="apellidos" class="form-control"/> 
                        Cédula: <input type="text" name="cedula" class="form-control"/>   
                        Direccion: <input type="text" name="direccion" class="form-control"/> 
                        Telefono: <input type="text" name="telefono" class="form-control"/> 
                        Fecha De Nacimiento: <input type="text" name="fecha_nacimiento" class="form-control"/> 
                        Email: <input type="text" name="email" class="form-control"/> 
                        <br/>
                        <input type="submit" value="Crear" class="btn btn-success"/>
                    </form>
                </div>
                <div class="col-lg-6 text-center">
                    <hr/>
                    <h3>Listado de servicios</h3>
                    <a href="controllers/controlador.php"><i class="fa fa-align-justify"></i> Acceder al listado de servicios</a>
                    <hr/>
                </div> 
            </div>
            <footer class="col-lg-12 text-center">
                Adaweb - <?php echo date("Y"); ?>
            </footer>
        </div>
    </body>
</html>